# The VS client script

## History

Pioneering VS on Linux with Tyron's support, I created an installer script for Linux that worked for me since VS 1.0.4 and was packaged with VS since 1.0.8. November 4th 2016. This script was part of a workaround to run the windows installer with WINE to extract the game folder, and relocate/adjust this folder to the needs of running the game natively with Mono on Linux. Luckily the extraction with WINE became obsolete since VS 1.2.1 on December 2nd 2016 as a tar.gz release became available.

As there still was no "real" client installer for linux, I coded a C#-based portable installer plus build script, meant as **Proof of Concept** for having a self-extracting/self-executing archive that could work on Windows and Linux. I posted this December 10th 2016 in the [forum.](https://www.vintagestory.at/forums/topic/116-portable-client-installer-for-linuxwindows/) 
But Tyron did not pick up this idea, so we have the tar.gz and the install.sh until now (as of October 2023 this is Version 1.18.15).

Unfortunately I am really bad with work-life-balance and stopped playing Computer Games for the next 8 months, drowning in my work as IT bloke. Thus, a couple of issues of my install.sh remained unfixed until my work project gave me break. November 12th 2017 I posted a reworked version of the install.sh to adress these issues, which went into VS 1.4.6.2. From December 2nd 2017 to January 13th 2018 I posted four updates based on issues, game changes and requests in the [forum.](https://www.vintagestory.at/forums/topic/241-updated-linux-installer-and-server-script/)

In this form, the install.sh script was working more or less unchanged over several years.

But with the switch to .NET 7 some script changes by the dev team accumulated, that are not consistent with the general approach of the script, probably due to the lack of the historical context of that script.

## Arch Linux and Standard Compliance

Because of the extensive coverage of the Arch Wiki, well-educated Arch users tend to notice quickly (and maybe even complain about) that VS does not follow the XDG Standards, expecially the XDG Base Directory Specification as Arch is interpreting and implementing this standard, right? Well, yes, you've been paying attention nicely!

Btw. Arch users should really install VS **via the AUR package** that copygirl maintains especially for you (go, check the VS Wiki, I honestly hope you give her some love for this), and they definitely not use this humble and primitive workaround install.sh script that was never meant to stay. I hope this answers all you questions and you can stop reading here as this is not your place.

So what about XDG? The X Desktop Group was a bold try to improve interoperability of the X Desktop Environments, and some environments like Gnome, KDE, or Xfce implement these recommendations more or less. The whole thing is not a standard, it is a more a close collaboration of some projects to achive a better integration of desktop applications. Because the Linux desktop has this insane degree of diversity that we all love and hate at the same time.

But sorry, I have to tell you that VS is not an open source Linux Desktop Application, it is a proprietary dotnet game, stemming from a Microsoft ecosystem. Dotnet, and not even Mono care the slightest about XDG integration recommendations. They care about some basic portability between the platforms Windows, Linux, and Mac on a high level, trying to define portable variables that work on all these platforms. And they define a 3rd-party application in a more portable and sandboxed way than a XDG desktop application.

Means: The path settings were originally derived from the standard dotnet variables in the way how WINE and Mono translate this to the linux system. There is no ` $XDG_DATA_HOME ` and no ` $XDG_CONFIG_HOME ` in dotnet. And how can you expect this to happen if these variables are not even set in open source flagship projects like Debian? In dotnet you have to consider something like
* ` appData    = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.UserProfile), Environment.GetFolderPath (Environment.SpecialFolder.ApplicationData)) `
* ` desktopDir = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.UserProfile), Environment.GetFolderPath (Environment.SpecialFolder.Desktop)) `
* ` fontFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Fonts) `

Maybe you understand now, e.g. why ` $HOME/.fonts ` used as the target for the font integration back in 2016 when a portable installer was expored, even this was considered as deprecated by XDG but well supported by all major linux distros. For platform interoperability this ancient standard is still a common and straightforward/reliable approach, considering e.g. FreeBSD as discussed here in [2021.]() Another point to consider is that these fonts are actually GAME RESOURCES and not meant to be shared in the desktop environment with other desktop applications. But of course this has to be re-evaluated for the switch to .NET 7. Means whatever System.Environment.SpecialFolder.Fonts will tell, if the fonts are recognized by the .NET libs then everything is nice at it should be.

Another point to consider: If the whole Linux community does not manage to provide a UNIFIED DESKTOP definition as a widely accepted industry standard for cross-platform projects like .NET, how shall a small indie studio be able to support all the different philosophies of the competeting sub-communities? Honestly, we should be thankful that Tyron was so kind to give us some basic Linux support. We really should not bother him with this common state of **criticism and disagreement** between the zealots and evangelists of the split Linux desktop world that tends to drag everyone down. 

You don't agree? It is not only the desktop, it is my beloved Linux. Just imagine Tyron would dare to integrate a server setup to automatically restart the server on reboot, aiming at a single solution that works for all linux distros and even FreeBSD. Can he rely on having SystemD and what is the alternative init system if not? This is only **one** reason why VS installing and launching a process on **system level** is out of scope. And this is not only because Linux seems to have some antagonizing isles of totalitarianism in its vast ocean of arnarchy. Btw. Poettering signed this XDG Base Directory Specification on top of the already existing Filesystem Hierarchy Standard which is actually a standard ...

**None** of these commonly cited standards provide a translation/migration path of the simple common categories that such a 3rd-party portable dotnet game would need: ApplicationData, Desktop, Fonts. And this is for a **good reason**: They set standards for **system managed** packages and **desktop apps** that are supposed to be installed centrally by the **root user**. They do not care the slightest about portable 3rd party stuff coming from windows ecosystems. Therefore this game has to be considered as **untrusted foreign code** that should only run in the **user space** and never-ever get access to the root level, because this could compromise the **whole** system security. Those Unix and Linux Systems are designed as secure **multi-user** server systems, you simply cannot translate a game from Windows single-user approach to this **system** level. At least not if you **really** understand how Unix and Linux are supposed to operate. Never forget: sudo is an abomination.

At the end of the day we all have to be humble and rely on the standard paths that portable frameworks like .NET provide us for the user space. Life's no bowl of cherries.


## Development Focus

Historically, the script was never meant as "real" installation method, it was meant to bridge the time until we have a 1st class citizen installation method that works at least on Linux as well. I always hoped that it will be eventually replaced by some kind of unified portable installation procedure that would cover all platforms (Windows, Linux, Mac).

Thus, my main goals were (ordered by priority)
1. keep it working and maybe adapt it according to Tyron's changes and requests if necessary
1. keep it backwards compatible with the simplistic user space approach (no sudo) and previous installations on the same machine 
1. keep it compatible with the official windows installer (based on dotnet environment variables as Mono/.NET implement them)
1. keep it as straightforward and minmalistic as possible: whole script must be reviewable on one screen page without scrolling
1. when possible: basic POSIX conformity, some basic logging, some code documentation (if it fits in the one-pager size)
1. when increasing complexity violates the targeted minimalism, a new codebase is needed, NOT SHELL! (like with the POC in C#)  
1. create and enhance documentation of the script and the use cases externally (e.g. this README)

Currently I take MX Linux 21 (Debian Bullseye) as the main reference for development and test of this script. I will probably switch to MX Linux 23 (Debian Bookworm) in the near future.

For monitoring the degree of POSIX conformity I use http://www.shellcheck.net/ for static analysis from version X on.
When this level is eventually ever passed passed (which I honestly doubt), I would extend testing with dash and POSIXLY_CORRECT flag and maybe on busybox-based alpine linux. But my expectation is that walking this path crosses the red line for the complexity fitting on the one page, driving me to switch to a complete different portable implementation (not shell-based).

## Script Installation Requirements

The client script (install.sh) can be used ....
It is a bash shell script, this means everybody can check the source code, nothing is hidden or obscure (provided bash shell knowlegde is available).

To start, simply download the setup script with your gnu/bash compatible computer into a folder of your choice:
* open a terminal, change to the desired directory, and execute:
  * **` wget https://gitlab.com/v-story/vsadmin/-/raw/master/client/install.sh `**
* make the script excutable the first time (if you do not know how to do this please start reading with [this tutorial](https://linuxconfig.org/bash-scripting-tutorial-for-beginners#h7-script-execution)).

You can also update the setup script this way.

The script needs as a minimum
* computer with internet access (at least to set everything up)
* wget package (at least version 1.17.9 that supports keypinning) used by the script itself, [e.g. 1.19.1 for ubuntu](http://security.ubuntu.com/ubuntu/pool/main/w/wget/wget_1.19.1-3ubuntu1.1_amd64.deb)
* mono package (at least version 4.2, latest stable version is highly recommended) used by Vinage Story

### Complete install
For a complete server/client install (option -C) capable to run the client too, the mono **complete** package is required (e.g. on a graphical desktop OS)
1. mono-complete

## Current Features

### Usage

* `install.sh [OPTION] `

### Setup options

* ` --minimal  `  do not create destop/menu starters

This option was requested by Cynthia January 2018 (probably as part of a security-driven code assessment at that time) and implemented in version X. However, Tyron did not include this version in the standard release, he took the previous script version without the requested switch instead.

## How to

The following instructions assume that the install.sh **client script** is already available on a Linux desktop environment that meets the minimal requirements (install.sh located in the VS directory that is extracted from the release tar.gz, or downloaded from the repot to this location).

1. **Install Vintage Story to play on Linux able to share worlds from the local server in your Home-LAN**
   * Check the requirements for a the installation to be fulfilled for the user who shall run the client on his desktop
   * Logged in as this user, open a terminal window, change to the directory of install.sh, and execute:
     * **` ./install.sh `**
   * Script result:
	 * Relocate the game installation to ` $HOME/ApplicationData/Vintagestory `
	 * Install the Vintagestory fonts in ` $HOME/.fonts `
	 * Install the desktop icon to start Vintagestory
   * If something does not work, please check ` $HOME/ApplicationData/Vintagestory/VintagestoryData/Logs/install.sh.log `
   * Run Vintagestory via desktop starter
 
## Roadmap

This is a list of potential topics for the next script versions.

From VS 1.9 onwards the install.sh was not maintained continously, some ad-hoc changes were introduced for unknown reasons
1. The game font location changed with adapting the install script for ages (happend somewhen in VS 1.9 stable)
1. Not only the additional collecting of the installer out put in a log files was disabled, but also the the cleanup of the old VS logs from the previous install (happened somewhen in VS 1.13 stable)
1.1. Possible reasoning: Installation logs are not needed for the maintenance (screenshots are sufficient in those cases) and cleaning up old logs als does not bring benefits for maintenance (log structure then is probably clean enough to not cause confusion). It could simplify the script and avoid edge cases for the initial installation (maybe ln -sf could fix one edge case).


### Prio 1 
1. Restore the minimalism for the .NET installation, eliminating the need for sudo commands. This non-installation tasks should be separated seperated as a requirements-checker/preparation script that could eventually help to fulfill the system requirements (not the scope of install.sh). Other tasks should probably separated as a tweak script, manipulating the system for better VS performance (not the scope of install.sh as well). Those other scripts shall be run
1.1. only if really needed
1.1. only once
1.1. only after previous inspection/confirmation by the user (so they must be really simple and transparent)

### Prio 2
1. consider ` $XDG_DATA_HOME ` and ` $XDG_CONFIG_HOME ` **when** it does not conflict with the special folder settings .NET **and** no other directory was used on the same machine before. If the freedom is given then, the default for data could be $HOME/.local/share (which usually contains user-specific data like fonts, application specific data). Userspace code could end in $HOME/.local/lib. 
Following this approach ` $XDG_CONFIG_HOME ` (default ` $HOME/.config `) is probably not needed, as there is no /etc like configuration as of now.
1. split update_client.sh from install.sh use case

### Prio 3
nothing so far

## How the Script Works

More info will be added later.

## FAQ

nothing so far
