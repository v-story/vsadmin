# Changelog

Overview of all changes in the client scripts for Vintage Story

## install.sh

### 2016-10-09 Script version 0.1 (included in VS 1.0.8 stable) 
* Workaround to install VS on linux after extracting the files from the windows installer via WINE
* Known Issue: dllmap configuration to be manually renamed: ` mv Lib/cairo-sharp.config Lib/cairo-sharp.dll.config `
* Known Issue: install.sh and Vintagestory.desktop contain windows-style line termination (CR = ^M)
* Known Issue: icon to be manually adjusted: ` convert assets/gameicon.ico assets/gameicon.xpm `
* Known Issue: execution rights to be set manually: ` chmod ug+x install.sh `

### 2016-12-02 Script version 0.2 (included in VS 1.2.1 stable)
* Feature: Tyron added a more Linux compatible release of the game to the account manager - a .tar.gz archive.
* Fixed: need to run the windows installer via WINE, just use the tar.gz instead
* Fixed: dllmap naming and gameicon.xmp within in the tar.gz
* Fixed: line termination and excutable bit of install.sh in the tar.gz

### 2017-11-12 Script version 0.3 (included in VS 1.4.6.2)
* Tweak: Optimize the script for working with the tar.gz instead of the extract from the windows installer (keeping the folder structure from that)
* Feature: Install.sh considers existing installations (install several versions in parallel, backup old installation, keep game data)
* Fixed: "CamelCase" sound files not found under linux (sometimes causes crashes, e.g. when entering a cave). Fix creates lowercase symbolic links to those sound files.
* Fixed: Missing game icon on the desktop, now pointing to assets/gameicon.xpm

### 2017-12-02 Script version 0.4 
* Feature: Due the world compatibility policy of 1.4.7.2 the installer now always copies the worlds from the existing version with the highest version number (means existing worlds should be automatically kept, but the old state is archived with the game version)
* Tweak: Path to be scanned for the need of filename links extened to all assets

### 2018-01-02 Script Version 0.5
* Fixed: Application directory will be created (was missing if the installer runs the first time)
* Tweak: Checking for the highest installed version number improved (was too restrictive, did not work for the transition 1.4.7.2 > 1.4.8.0)

### 2018-01-06 Script Version 0.6
* Tweak: Using new -v option instead of monodis (eliminates one dependency and its check)
* Feature: Check for mono-xmltool (package mono-complete) and compiler version > 4.2 (requested)
* Feature: Environment-Variable ` NO_SUPPORT_MODE ` to possibly ignore the version check (requested)

### 2018-01-13 Script Version 0.7
* Feature: Update of install.sh to support the client changes of 1.4.9.1 & 1.4.9.2 (splitting installation & backups over different paths)
* Feature: derive default DataPath directly from Mono to ensure future compatibility with windows updater
* Fixed: Typo in application folder variable, preventing the folder to be created when missing
* Feature: If you do not like the *.desktop files, use "install.sh --minimal" (requested)

### 2018-02-06 Script Version 0.8
* Tweak: Always the same install folder on re-install/update: no renaming per VS version, also eliminating the need to actually replace existing destop-starters each time.
* Known Issue: This behavior still differs from the windows installer as of 1.5.0.4 (this installer allows to SELECT an installation folder)

### 2018-03-12 Script Version 0.9 (included in VS 1.5.2 stable)
* Known Issue: There are cases, that the XGD desktop icons do not work (reason is unclear, can be non-compliant desktop, can be security feature, to disable execution after changing the binary)
* Feature: Completion message that tells the user a workaround if the desktop starter does not work (requested)

### 2018-05-19 Script Version 1.0 (included in v1.9-rc.1)
* Tweak: Bump Mono requirements from 4.2 to 5.1 for the upcoming Worldgen and Geology Update (requested for better support)
* Known Issue: some Mono distributions come without proper root certificates, these need to be installed seperately as root. The installer is not supposed to do this. I did analysis on that issue but had no cpapacity to script a possible distro-specific requirements-pre-installer and discussed this with Tyron. We went then for the instruction workaround that Tyron added himself."
* Tweak: Extend completion message to teach the user how to fix a possible cert issue (workaround)

### 2020-09-15 Script Version 1.1 (included somewhen in v1.13)
* Known Issue: since 1.9 stable, the game fonts are located under ` ./assets/game/fonts ` but the installer is still not adapted to that
* Tweak: removing installion log and cleanup of lod application logs on version update

### 2022-08-22 Script Version 1.2 (included somewhen in v1.17)
* Tweak: cosmetic change of font location to ./local/share/fonts (requested by AUR package user)
* Fixed: installer to consider that game fonts are located under ` ./assets/game/fonts ` (known issue since 1.9)
* Fixed: Eliminate edge case in Menustarter/Deskstarter logic by creating the menustarter base directory
* Tweak: make above change less redundant (using existing variables and existing directory creation loop)
* Tweak: printf with line breaks for success message

### 2024-05-20 Script Version 1.3
* Tweak for dotnet compatibility: get Deskstarter path from System.Environment.SpecialFolder.Desktop
* Tweak for dotnet compatibility: get (local) font location from System.Environment.SpecialFolder.Fonts

