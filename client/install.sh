#!/bin/bash
## archive="vs_client_$(wget 'http://version.vintagestory.at/version.txt' -q -O -).tar.gz"; wget "https://account.vintagestory.at/files/stable/${archive}" -q
## check installation prequisite
[ $(dotnet --list-runtimes 2>/dev/null | grep 'Microsoft.NETCore.App' | tr -dc '[:digit:]' | cut -c-2) -ge 70 ] || { echo "Need dotnet 7, please install (https://dotnet.microsoft.com/en-us/download/dotnet/7.0). Aborting." >&2; exit 1; }
hash xdg-desktop-menu && hash envsubst && [ "${1}" != "--minimal" ] && STARTERS=true

## current location of this script
FILE_PATH="$(readlink -f -- "$0")"
WORK_PATH="$(dirname -- "$FILE_PATH")"

## consider predefined installation targets (INST_DIR, DATA_PATH, USERFONTS, optionally DESKSTARTER)
. "${FILE_PATH%.sh}.conf"
[ -n "${INST_DIR}" ] && { export INST_DIR; echo; echo "Install game into ${INST_DIR} (data into ${DATA_PATH})"; }

## create needed directories
for i in $DATA_PATH/Logs ${INST_DIR%/*} ${USERFONTS} ; do [ -d "$i" ] || { echo "Create new dir $i"; mkdir -p "$i"; }; done

## set list separator to enable iteration over filenames with spaces
IFS=$(echo -en "\n\b")
## relocate installation if not already in the right place
if [ "$(readlink -f -- "${INST_DIR}")" != "$(readlink -f -- "${PWD}")" ]; then
  ## switch to working directory
  cd "${WORK_PATH}"
  ## backup old installation
  [ -d "${INST_DIR}" ] && { echo "Backup existing ${INST_DIR}"; mv -f "${INST_DIR}" "${INST_DIR}.bak.$(date '+%Y%m%d%H%M%S')"; }
  if [ "${1}" != "--minimal" ]; then
    ## copy latest backup game directories that are not packaged (e.g. user specific or from legacy setup)
    for i in $(find $(ls -d ${INST_DIR}.* | sort -d | tail -n 1) -mindepth 1 -maxdepth 1 -type d)
		do [ -d "./${i##*/}" ] || { echo "Keep old dir ${i##*/}"; cp -pfr "$i" "./${i##*/}"; }; done
    ## handle legacy world location
    if [ -d ./VintagestoryData -a "${DATA_PATH}" != "${INST_DIR}/VintagestoryData" ]; then
      { echo "Move Data"; cp -pfrl ./VintagestoryData "${DATA_PATH%/*}" && rm -fr ./VintagestoryData; }; fi
  fi
   ## move the game directory 
  { echo "Move new application dir ${WORK_PATH}"; mv -f "${WORK_PATH}" "${INST_DIR}"; }
fi
cd "${INST_DIR}" && echo "Working over installed ${INST_DIR}"
## check/install game *.ttf and *.otf fonts
for i in $(find "./assets/game/fonts" '(' -name "*.ttf" -o -name "*.otf" ')' -type f); do [ -f "${USERFONTS}/${i##*/}" ] || { echo "Install game font ${i##*/}"; cp -p "${i}" "${USERFONTS}"; }; done
## apply fixes for known issues
for i in $(find "./assets"); do f="$(basename -- "$i")"; [ "$f" = "${f,,}" ] || { echo "Create lowercase alias for $f"; ln -sf "$f" "${i%/*}/${f,,}"; }; done
## check/install desktop starters if possible
if [ ${STARTERS} ]; then
  echo "Create menu starter plus URL handlers to connect and install mods"
  for i in ./Vintagestory*.desktop; do
    envsubst < $i > $i.tmp && mv $i.tmp $i
    xdg-desktop-menu install --novendor "$i" 
  done
  [ -n "${DESKSTARTER}" ] && [ -f ./Vintagestory.desktop ] && cp -pfr ./Vintagestory.desktop ${DESKSTARTER} && chmod ugo+x ${DESKSTARTER}
fi

printf "Install complete. If the desktop icon does not work, you can still run the game by executing './Vintagestory'.\n\n"
