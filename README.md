# VSadmin

This repository contains admin scripts for Vintage Story (please take a look at the [homepage](https://www.vintagestory.at) if you want to learn more about Vintage Story) to be executed in a terminal or to be integrated in automated tasks of the server host OS. 

## History

Being a Linux dude with more than 20 year expertise as IT professional, I bought VS with Supporter Addon at "day one" end of September 2016 when Linux was not even supported. Why? I discovered VS in April 2016 when Tyron posted his journey and I knew in my heart that this will become the better-than-minecraft game I was waiting for. And I was sure that I can make it run natively under Linux, as VS is a C# project originally based on the Manic Digger engine (MIT License) which Tyron completely rewrote over time. And he did a great job with that.

For the **client**, I started by running the proprietary windows installer with WINE in order extract the files. From VS 1.0.2  on I tinkered with the support from Tyron to make it work with Mono, exchanging hints and findings (mostly about the needed library mappings on Mono). Then, October 21st 2016 I was able to play VS 1.0.4 natively on Linux. Four days later, I posted a very first version of a script and configs and Tyron decided to include the compatibility configs in the game. A few days later, I scripted a **preliminary install.sh** that Tyron **included in VS 1.0.8**. A couple of relases later, at December 2nd 2016, came the linux-friendly VS 1.2.1 with tar.gz download option, eliminating the need to run a windows-installer with WINE. VS on Linux was born!

For the **server**, I started to use the multiplayer feature from 1.4.6.1 on about one year later, still one of the few early pioneers with VS on Linux, using the server.sh packaged with VS to set up a local family server in my LAN. So I could give frequent testing feedback about this feature, that was not so heavily focused at this time. Tyron asked me at some point in time if I could do some needed fixes on the server.sh and test them out. As the forums became unhandy to exchange the information, he asked me to set up a git repo for the server.sh.

## Server Script

The purpose of the server script is to set up and manage a VS server on a linux machine.

## Client Script

The purpose of the client script is to install the VS client on a linux desktop computer.
