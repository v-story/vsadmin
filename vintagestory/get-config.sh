#!/bin/bash
## check installation prequisite
hash csharp 2>/dev/null || { echo "Please install recent csharp before running this script."; exit 1; }

## current location of this script
FILE_PATH="$(readlink -f -- "$0")"
WORK_PATH="$(dirname -- "$FILE_PATH")"
DATA_PATH="$(csharp -e 'print(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));' 2>/dev/null)/VintagestoryData"

## define installation targets
APP_DATA="$HOME/ApplicationData"
APP_NAME="$(basename -- "${WORK_PATH%%.*}")" && { echo "Create config for game in ${APP_DATA}/${APP_NAME} (data into ${DATA_PATH})"; }
USERFONTS="$(csharp -e 'print(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Fonts));')"
DESKSTARTER="$(csharp -e 'print(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop));')/${APP_NAME}.desktop"

## create a config file
CFG="install.conf"
printf "### installation paths ###\n" > ${CFG}
printf "DATA_PATH=${DATA_PATH/"$HOME"/"~"}\n" >> ${CFG}
printf "USERFONTS=${USERFONTS/"$HOME"/"~"}\n" >> ${CFG}
printf "DESKSTARTER=${DESKSTARTER/"$HOME"/"~"}\n" >> ${CFG}
printf "INST_DIR=${APP_DATA/"$HOME"/"~"}/${APP_NAME}\n" >> ${CFG}

# xdg-user-dir DESKTOP
